fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## iOS

### ios certs

```sh
[bundle exec] fastlane ios certs
```

Fetches the provisioning profiles so you can build locally and deploy to your device

### ios generate_certs

```sh
[bundle exec] fastlane ios generate_certs
```

Generate certificates

### ios beta

```sh
[bundle exec] fastlane ios beta
```

Description of what the lane does

### ios check_ci

```sh
[bundle exec] fastlane ios check_ci
```



----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
